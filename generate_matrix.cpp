//
// Created by Max Nigmatulin on 15.11.2022.
//

#include "generate_matrix.h"
#include <random>


float **generate_matrix(int size) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dist(0, 1);

    float **a = new float *[size];
    for (int i = 0; i < size; i++) {
        a[i] = new float[size];
    }
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (i == j) {
                a[i][j] = dist(gen);
            } else {
                a[i][j] = 0;
            }
        }
    }

    return a;
}

float *generate_vector(int size) {
    float *v = new float[size];
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> dist(1, 10000000); // define the range

    for (int i = 0; i < size; i++) {
        v[i] = dist(gen);
    }

    return v;
}
