import subprocess
from datetime import datetime
from pathlib import Path
import os
from shutil import rmtree


class Experiment:

    def __init__(self, exec_path: str, threads: int, rank: int, iterations_num: int, exp_filename: str) -> None:
        self.exec_path = exec_path
        self.threads = threads
        self.rank = rank
        self.iteration_num = iterations_num
        self.exp_filename = exp_filename

    def launch(self):
        subprocess.call(
            [str(item) for item in [self.exec_path, self.threads, self.rank, self.iteration_num, self.exp_filename]]
        )

    def print(self):
        print(
            [str(item) for item in [self.exec_path, self.threads, self.rank, self.iteration_num, self.exp_filename]]
        )


# task 1
def construct_rank_experiment(
        exec_path, ranks: [int], threads: [int], iterations_num: int, exp_filename_base="ranks_experiment"
) -> [Experiment]:
    experiments = []

    for rank in ranks:
        for thread in threads:
            experiments.append(
                Experiment(
                    exec_path=exec_path,
                    threads=thread,
                    rank=rank,
                    iterations_num=iterations_num,
                    exp_filename=f"{exp_filename_base}_rank={rank}.txt"
                )
            )
    return experiments


# task 2
def construct_iterations_experiment(
        exec_path, iterations: [int], threads: [int], rank: int, exp_filename_base="iterations_experiment"
) -> [Experiment]:
    experiments = []
    for iterations_num in iterations:
        for thread in threads:
            experiments.append(
                Experiment(
                    exec_path=exec_path,
                    threads=thread,
                    rank=rank,
                    iterations_num=iterations_num,
                    exp_filename=f"{exp_filename_base}_iters={iterations_num}.txt"
                )
            )
    return experiments


def construct_amdahls_experiment(
        exec_path, threads: [int], iterations_num: int, rank: int, exp_filename_base="amdahls_experiment"
) -> [Experiment]:
    experiments = []
    for thread in threads:
        experiments.append(
            Experiment(
                exec_path=exec_path,
                threads=thread,
                rank=rank,
                iterations_num=iterations_num,
                exp_filename=f"{exp_filename_base}.txt")
        )

    return experiments


def create_dir(path):
    if os.path.exists(path):
        rmtree(path)
    Path(path).mkdir(parents=True, exist_ok=True)


HOME_SYSTEM = True


def main():
    exec_symbol = "./"
    exec_name = "a.out"
    if HOME_SYSTEM:
        compiler = "/opt/homebrew/opt/llvm/bin/clang++"
        available_thread_num = 8
    else:
        compiler = "g++"
        available_thread_num = 12
    build_exec = [compiler, "main.cpp", "generate_matrix.cpp", "-fopenmp", "-O0"]

    def time():
        return datetime.now().strftime("%H:%M:%S")

    experiment_threads = [i for i in range(1, available_thread_num + 1)]

    create_dir("./ranks")
    thread_experiment_ranks = [100, 300, 700, 1500]
    ranks_experiments = construct_rank_experiment(
        exec_path=exec_symbol + exec_name,
        ranks=thread_experiment_ranks,
        threads=experiment_threads,
        iterations_num=10 ** 4,
        exp_filename_base="./ranks/ranks_exp"
    )

    create_dir("./iter")
    thread_experiment_iterations = [10 ** 3, 10 ** 4, 10 ** 5, 10 ** 6]
    iterations_experiments = construct_iterations_experiment(
        exec_path=exec_symbol + exec_name,
        iterations=thread_experiment_iterations,
        threads=experiment_threads,
        rank=200,
        exp_filename_base="./iter/iterations_exp"
    )

    amdahls_law_check = construct_amdahls_experiment(
        exec_path=exec_symbol + exec_name,
        threads=experiment_threads,
        iterations_num=10 ** 3,
        rank=3000,
        exp_filename_base="amdahls_law_check"
    )

    subprocess.call(build_exec)

    print(time() + ": ranks experiment")
    for experiment in ranks_experiments:
        experiment.launch()

    print(time() + ": iterations experiment")
    for experiment in iterations_experiments:
        experiment.launch()

    print(time() + ": amdahls law check")
    for experiment in amdahls_law_check:
        experiment.launch()


if __name__ == "__main__":
    main()
