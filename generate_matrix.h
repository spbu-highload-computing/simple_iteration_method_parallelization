//
// Created by Max Nigmatulin on 15.11.2022.
//

#ifndef HIGHLOAD_COMPUTING_GENERATE_MATRIX_H
#define HIGHLOAD_COMPUTING_GENERATE_MATRIX_H

float **generate_matrix(int size);

float *generate_vector(int size);

#endif //HIGHLOAD_COMPUTING_GENERATE_MATRIX_H
