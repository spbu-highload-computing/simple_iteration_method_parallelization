#include <iostream>
#include <fstream>
#include <omp.h>
#include <cstring>
#include "generate_matrix.h"

float *simple_iterations_method(float **a, float *b, int n, int iterations = 10) {

    float **H = new float *[n];
    for (int i = 0; i < n; i++) {
        H[i] = new float[n];
    }
    float *g = new float[n];

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i == j) {
                H[i][j] = 0;
            } else {
                H[i][j] = -1 * a[i][j] / a[i][i];
            }
        }
        g[i] = b[i] / a[i][i];
    }
    float *x = new float[n];

        for (int k = 0; k < iterations; k++) {
            float *new_x = new float[n];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    new_x[i] += H[i][j] * x[j];
                }
                new_x[i] += g[i];
            }
            x = new_x;
        }
    return x;
}

float *simple_iterations_method_parallel(float **a, float *b, int n, int iterations = 10, int num_threads = 2) {

    float **H = new float *[n];
    for (int i = 0; i < n; i++) {
        H[i] = new float[n];
    }
    float *g = new float[n];

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i == j) {
                H[i][j] = 0;
            } else {
                H[i][j] = -1 * a[i][j] / a[i][i];
            }
        }
        g[i] = b[i] / a[i][i];
    }
    float *x = new float[n];

#pragma omp parallel shared(H, g, n, iterations, x, std::cout) default(none) num_threads(num_threads)
    // OMP разбивает на блоки по <iterations / num_threads> индексов если паралеллить этот for
    for (int k = 0; k < iterations; k++) {
        float *new_x = new float[n];

#pragma omp for
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                new_x[i] += H[i][j] * x[j];
            }
            x[i] = new_x[i] + g[i];
        }
    }

    return x;
}

int main(int argc, char *argv[]) {
    // 0: program path
    // 1: thread num
    // 2: matrix rank
    // 3: iteration count
    // 4: stats_filename
    int thread_num = atoi(argv[1]);
    int matrix_rank = atoi(argv[2]);
    int iterations = atoi(argv[3]);
    char *stats_filename = argv[4];
    std::ofstream stats_file(stats_filename, std::ios::app);

    auto a = generate_matrix(matrix_rank);
    auto b = generate_vector(matrix_rank);

    double time_start, time_finish;

    time_start = omp_get_wtime();
    float *result;
    if (thread_num == 1) {
        result = simple_iterations_method(a, b, matrix_rank, iterations);
    } else {
        result = simple_iterations_method_parallel(a, b, matrix_rank, iterations, thread_num);
    }
    time_finish = omp_get_wtime();

    stats_file << "(threads=" << thread_num << "): ";
    stats_file << "rank=" << matrix_rank << " ";
    stats_file << "iterations=" << iterations << " ";
    stats_file << "time=" << time_finish - time_start << std::endl;
    stats_file.close();
}
